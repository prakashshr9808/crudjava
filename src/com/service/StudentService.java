package com.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.connection.DbConnection;
import com.model.Student;

public class StudentService {
	
	public void registerStudent(Student s) throws ClassNotFoundException, SQLException {
		
		DbConnection conClass = new DbConnection();
		Connection con = conClass.getConnection();
		
		Statement st = con.createStatement();
		
		PreparedStatement pst = con.prepareStatement("insert into newcrud (firstname,lastname,password) values(?,?,?)");
		pst.setString(1,s.getFirstname());
		pst.setString(2, s.getLastname());
		pst.setString(3, s.getPassword());
		
		pst.executeUpdate();		
	}
	
}
