package com.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	
	public static String url="jdbc:mysql://localhost:3306/crudpractic";
	public static String user="root";
	public static String password="";
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.driver");
		Connection con = DriverManager.getConnection(url,user,password);
		return con;
	}
	
}
